# pa-hello-world-app

## Commands

## Node Install
`npm install`

### Node app
- Run the application locally.
`node index.js`

### Docker

- Docker build
`docker build -t pa-hello-world-app .`
- Create and Run the Container
`docker run -p 3001:3001 pa-hello-world-app:latest`
- Upload the image to Docker Registry - DockerHub
  - Tag the image
  `docker tag pa-hello-world-app mawantha/pan-asia-hello-world:1.0`
  - Push it to DockerHub
  `docker push mawantha/pan-asia-hello-world:1.0`
  
### Kubernetes
- Update the `deployment.yaml` file in containers category with image repository url
`image: mawantha/pan-asia-hello-world:1.0`
- Create the deployment.
`kubectl create -f <filename>.yaml`
- Check the deployment and the pods
`kubectl get deploy,po`
- Expose the deployment to the internet via loadbalancer
`kubectl expose deployment nodejs-deployment --type="LoadBalancer"`
- Check the service
`kubectl get svc`

🎉🎉 You have successfully deployed your container image to Kubernetes!!

